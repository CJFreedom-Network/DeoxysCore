package uk.co.atlasmedia.commands;

import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import uk.co.atlasmedia.player.Rank;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * Created by James on 5/26/2017.
 */
public class CommandRegistrar
{
    CommandMap cmap;

    public CommandRegistrar() throws IOException
    {
        registerCommands();
    }

    private void registerCommands() throws IOException
    {
        ClassPath cp = ClassPath.from(getClass().getClassLoader());
        cp.getTopLevelClassesRecursive("uk.co.atlasmedia").forEach(classInfo ->
        {
            try
            {
                if (classInfo.getName().startsWith("Command_")) //if this doesnt work, change to contains(string)
                {
                    Class clazz = Class.forName(classInfo.getName());
                    Object classObject = clazz.newInstance();
                    if (classObject instanceof DCommand)
                    {
                        DCommand cmd = (DCommand) classObject;
                        Annotation annotation = clazz.getAnnotation(CommandInformation.class);
                        CommandInformation c = (CommandInformation) annotation;
                        cmd.setName(c.name());
                        if (c.description() != null) cmd.setDescription(c.description()); //not required
                        cmd.setUsage(c.usage());
                        if (c.aliases() != null) cmd.setAliases(c.aliases()); //not required
                        cmd.setRank(c.rank());
                        registerCommand(cmd);
                    }
                }
            }
            catch (NullPointerException e) //TODO Add class specific error catching, so that you can say "there is a problem with thisClass.java"
            {
                Bukkit.getLogger().severe("Error registering " + classInfo.getSimpleName() + ". Check that the command's name, usage, and rank @CommandInformation fields are filled out.");
            }
            catch (Exception e)
            {
                Bukkit.getLogger().severe("Error registering " + classInfo.getSimpleName() + ".");
                e.printStackTrace();
            }
        });
    }

    public void registerCommand(DCommand cmd) //needs cleanup
    {
        BlankCommand blankCommand = new BlankCommand(cmd.getName());
        if (cmd.getAliases() != null) blankCommand.setAliases(Arrays.asList(cmd.getAliases()));
        if (cmd.getDescription() != null) blankCommand.setDescription(cmd.getDescription());
        blankCommand.setUsage(cmd.getUsage());
        blankCommand.setCmd(cmd);
        getCommandMap().register("", blankCommand);
    }

    final CommandMap getCommandMap()
    {
        if(cmap == null)
        {
            try
            {
                final Field f = Bukkit.getServer().getClass().getDeclaredField("commandMap");
                f.setAccessible(true);
                cmap = (CommandMap) f.get(Bukkit.getServer());
                return getCommandMap();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        else if(cmap != null)
        {
            return cmap;
        }
        return getCommandMap();
    }


    public static abstract class DCommand implements CommandExecutor
    {
        @Getter @Setter public String name;
        @Getter @Setter public String description;
        @Getter @Setter public String usage;
        @Getter @Setter public String[] aliases;
        @Getter @Setter public Rank rank;

        public abstract boolean onCommand(CommandSender sender, Command cmd, String label, String[] args);
    }

    public class BlankCommand extends Command
    {
        @Setter DCommand cmd;

        protected BlankCommand(String name)
        {
            super(name);
        }

        @Override
        public boolean execute(CommandSender commandSender, String s, String[] strings)
        {
            if (cmd == null)
            {
                return false;
            }

            if (!Rank.isRankOrHigher(commandSender, cmd.getRank()))
            {
                commandSender.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
                return false;
            }

            if (!cmd.onCommand(commandSender, this, s, strings))
            {
                commandSender.sendMessage(ChatColor.RED + "Usage: " + cmd.getUsage().replaceAll("<command>", cmd.getName()));
                return false;
            }
            return true;
        }
    }
}

package uk.co.atlasmedia.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import uk.co.atlasmedia.player.Rank;

import uk.co.atlasmedia.commands.CommandRegistrar.*;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Created by James on 5/27/2017.
 */

@CommandInformation(name="test", description="testing testing 123", usage="/<command>", aliases="1", rank= Rank.MEMBER)
public class Command_test extends DCommand
{
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        Bukkit.broadcastMessage("test");
        return true;
    }
}

package uk.co.atlasmedia.commands;

import uk.co.atlasmedia.player.Rank;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by James on 5/26/2017.
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface CommandInformation
{
    String name();

    String description();

    String usage();

    String[] aliases() default "";

    Rank rank() default Rank.MEMBER;
}

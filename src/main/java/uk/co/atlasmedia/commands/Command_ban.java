package uk.co.atlasmedia.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import uk.co.atlasmedia.commands.CommandRegistrar.DCommand;
import uk.co.atlasmedia.player.Rank;

/**
 * Created by James on 5/26/2017.
 */

@CommandInformation(name = "ban", description = "Ban a player permanently.", usage = "/<command> <player> <reason>", rank = Rank.SENIOR_ADMIN)
public class Command_ban extends DCommand
{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        return false;
    }
}

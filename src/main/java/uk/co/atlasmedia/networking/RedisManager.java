package uk.co.atlasmedia.networking;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;
import uk.co.atlasmedia.DeoxysCore;

//import com.lambdaworks.redis.api.sync.RedisStringCommands;
//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.JedisPool;
//import redis.clients.jedis.JedisPoolConfig;
//import redis.clients.jedis.exceptions.JedisConnectionException;

public class RedisManager
{
    //public JedisPool pool;

    private RedisURI redisUri;

    RedisClient client;

    StatefulRedisConnection<String, String> connection;

    RedisCommands sync;

    //Database 0 = rank
    //Database 1 = coins
    //Database 2 = mutes
    //Database 3 = online players/current server //TODO Add current server

    public RedisManager(String hostname, int port, String password)
    {
        /*this.hostname = hostname;
        this.port = port;
        this.password = password; //Might throw a NPE
        JedisPoolConfig config = new JedisPoolConfig();
        config.setTestOnBorrow(false);
        config.setTestOnReturn(false);
        config.setTestWhileIdle(false);*/

        if (password != null)
        {
            redisUri = RedisURI.Builder.redis(hostname)
                .withPort(port)
                .withPassword(password)
                .withDatabase(0)
                .build();
        }
        else
        {
            redisUri = RedisURI.Builder.redis(hostname)
                    .withPort(port)
                    .withDatabase(0)
                    .build();
        }
        client = RedisClient.create(redisUri);
        connection = client.connect();
        sync = connection.sync();
    }

    public Object getValue(String key, int database)
    {
        try
        {
            sync.select(database);
            return sync.get(key);
        }
        catch (Exception e)
        {
            DeoxysCore.plugin.getLogger().severe("Could not establish connection to Redis server.");
        }
        return null;
    }

    public void setValue(String key, String value, int database)
    {
        try
        {
            sync.select(database);
            sync.set(key, value);
        }
        catch (Exception e)
        {
            DeoxysCore.plugin.getLogger().severe("Could not establish connection to Redis server.");
        }
    }

    public int getInteger(String key, int database)
    {
        try
        {
            sync.select(database);
            return new Integer((String) sync.get(key));
        }
        catch (Exception e)
        {
            DeoxysCore.plugin.getLogger().severe("Could not establish connection to Redis server.");
        }
        return -1;
    }

    public void deleteObject(String key, int database)
    {
        try
        {
            sync.select(database);
            sync.del(key);
        }
        catch (Exception e)
        {
            DeoxysCore.plugin.getLogger().severe("Could not establish connection to Redis server.");
        }
    }

    public boolean isMutedRedis(String uuid)
    {
        return getValue(uuid, 2) != null;
    }

    public String getMuteExpiryTimeRedis(String uuid)
    {
        if (!isMutedRedis(uuid)) return null;

        return (String) getValue(uuid, 2);
    }
}

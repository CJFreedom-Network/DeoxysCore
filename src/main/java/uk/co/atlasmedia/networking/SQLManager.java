package uk.co.atlasmedia.networking;

import uk.co.atlasmedia.DeoxysCore;

import java.sql.*;
import java.util.logging.Level;

//Portions of this class were created by -_Husky_-, tips48, and Camzie.
public class SQLManager
{
    //Credit: Husky and tips48.
    private final String user;
    private final String database;
    private final String password;
    private final String port;
    private final String hostname;

    private Connection connection;

    /**
     * Creates a new MySQL instance
     *
     * @param hostname Name of the host
     * @param port Port number
     * @param database Database name
     * @param username Username
     * @param password Password
     */
    public SQLManager(String hostname, String port, String database, String username, String password)
    {
        this.hostname = hostname;
        this.port = port;
        this.database = database;
        this.user = username;
        this.password = password;
    }

    public Connection openConnection()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + this.hostname + ":" + this.port + "/" + this.database, this.user, this.password);
        }
        catch (SQLException e)
        {
            DeoxysCore.plugin.getLogger().log(Level.SEVERE, "Could not connect to MySQL server! because: " + e.getMessage());
        }
        catch (ClassNotFoundException e)
        {
            DeoxysCore.plugin.getLogger().log(Level.SEVERE, "JDBC Driver not found!");
        }
        return connection;
    }

    public boolean checkConnection()
    {
        return connection != null;
    }

    public Connection getConnection()
    {
        if (!checkConnection())
        {
            DeoxysCore.plugin.getLogger().info("Opening MySQL connection.");
            openConnection(); //TODO Remove the SQL connection message.
        }
        try
        {
            if (connection.isClosed())
            {
                openConnection();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return connection;
    }

    public void closeConnection()
    {
        if (connection != null)
        {
            try
            {
                connection.close();
            }
            catch (SQLException e)
            {
                DeoxysCore.plugin.getLogger().log(Level.SEVERE, "Error closing the MySQL Connection!");
                e.printStackTrace();
            }
        }
    }

    //Credit: Camzie

    //TODO Cleanup. These methods should utilize the PreparedStatement utilities for variables inTable and uniqueColumn to prevent possible SQLi.

    public Object getFromTable(String uniqueColumn, String uniqueValue, String lookingFor, String inTable) throws SQLException
    {
        Connection c = getConnection();
        PreparedStatement statement = c.prepareStatement("SELECT * FROM `" + inTable + "` WHERE `" + uniqueColumn + "` = ?");
        statement.setString(1, uniqueValue);
        ResultSet res = statement.executeQuery();
        Object results;
        if(res.next())
        {
            results =  res.getObject(lookingFor);
            res.close();
            statement.close();
            //closeConnection();
            return results;
        }
        res.close();
        statement.close();
        //closeConnection();
        return null;
    }

    public boolean updateInTable(String uniqueColumn, String uniqueValue, Object newValue, String columnToChange, String table) throws SQLException
    {
        Connection c = getConnection();
        PreparedStatement statement = c.prepareStatement("UPDATE " + table + " SET " + columnToChange + " = ? WHERE " + uniqueColumn + " = ?");
        statement.setObject(1, newValue);
        statement.setString(2, uniqueValue);
        int i = statement.executeUpdate();
        statement.close();
        //closeConnection();
        return i > 0;
    }

    //Ours

    public void deleteFromTable(String uniqueColumn, String uniqueValue, String inTable) throws SQLException
    {
        Connection c = getConnection();
        PreparedStatement statement = c.prepareStatement("DELETE FROM `" + inTable + "` WHERE `" + uniqueColumn + "` = ?");
        statement.setString(1, uniqueValue);
        statement.executeUpdate();
        //closeConnection();
    }

    public boolean playerExists(String uuid) throws SQLException
    {
        return getFromTable("uuid", uuid, "uuid", "players") != null;
    }

    //Bans

    public void addNameBan(String uuid, String name, String banned_by, String reason) throws SQLException
    {
        Connection c = getConnection();
        PreparedStatement statement = c.prepareStatement("INSERT INTO `banned_players` (uuid, name, banned_by, banned_on, reason) VALUES (?, ?, ?, ?, ?)");
        statement.setString(1, uuid);
        statement.setString(2, name);
        statement.setString(3, banned_by);
        statement.setLong(4, new java.util.Date().getTime());
        statement.setString(5, reason);
        statement.executeUpdate();
    }

    //Mutes

    public boolean isMutedSQL(String uuid) throws SQLException
    {
        return getFromTable("uuid", uuid, "uuid", "mutes") != null;
    }

    public String getExpiryTimeSQL(String uuid) throws SQLException
    {
        if (!isMutedSQL(uuid)) return null;

        return getFromTable("uuid", uuid, "expires_on", "mutes").toString();
    }

    public void setMuted(String uuid, Timestamp expiry) throws SQLException
    {
        Connection c = getConnection();
        PreparedStatement statement = c.prepareStatement("INSERT INTO `mutes` (`uuid`, `expires_on`) VALUES (?, ?)");
        statement.setString(1, uuid);
        statement.setTimestamp(2, expiry);
        statement.executeUpdate();
        statement.close();
        //closeConnection();
    }

    //Everything below here is ours.
}

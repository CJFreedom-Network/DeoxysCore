package uk.co.atlasmedia;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;
import uk.co.atlasmedia.commands.CommandRegistrar;
import uk.co.atlasmedia.networking.RedisManager;
import uk.co.atlasmedia.networking.SQLManager;

import java.io.IOException;

/**
 * Created by James on 5/26/2017.
 */
public class DeoxysCore extends JavaPlugin
{
    public static DeoxysCore plugin;

    public SQLManager sqlManager;
    public RedisManager redisManager;

    CommandRegistrar commandRegistrar;

    @Override
    public void onEnable()
    {
        plugin = this;
        try
        {
            sqlManager = new SQLManager("localhost", "3306", "deoxyscore", "root", "SVcP94IhvMk1hgo29lW");
            redisManager = new RedisManager("192.168.1.22", 6379, null);
        }
        catch (Exception e)
        {
            Bukkit.getLogger().severe("Could not establish connection to MySQL/Redis. Check credentials and try again.");
            Bukkit.shutdown();
        }

        try
        {
            commandRegistrar = new CommandRegistrar();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}

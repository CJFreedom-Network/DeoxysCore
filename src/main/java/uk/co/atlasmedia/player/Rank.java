package uk.co.atlasmedia.player;

import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import uk.co.atlasmedia.DeoxysCore;

import java.sql.SQLException;

/**
 * Created by James on 5/26/2017.
 */
public enum Rank
{
    MEMBER("Member", 1, ChatColor.RED, "Member"),
    SUPER_ADMIN("Admin", 2, ChatColor.DARK_AQUA, "SA"),
    SENIOR_ADMIN("Senior Admin", 3, ChatColor.AQUA, "SrA"),
    EXECUTIVE("Executive", 4, ChatColor.DARK_GREEN, "Exec"),
    MANAGER("Manager", 5, ChatColor.DARK_RED, "Mgr");

    @Getter public String name;
    @Getter public int level;
    @Getter public ChatColor color;
    @Getter public String tag; //Admin-chat tag

    Rank(String name, int level, ChatColor color, String tag)
    {
        this.name = name;
        this.level = level;
        this.color = color;
        this.tag = tag;
    }

    public static Rank getRank(CommandSender sender)
    {
        if (sender instanceof ConsoleCommandSender)
        {
            return Rank.SENIOR_ADMIN;
        }
        else
        {
            Player player = (Player) sender;
            try
            {
                String uuid = player.getUniqueId().toString();
                if (!DeoxysCore.plugin.sqlManager.playerExists(uuid))
                {
                    return null;
                }

                String rankStr = (String) DeoxysCore.plugin. //TODO Ranks through redis

            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
        return Rank.MEMBER;
    }

    public static boolean isRankOrHigher(CommandSender sender, Rank rank)
    {
        return rank.level <= getRank(sender).getLevel();
    }
    public static boolean isRank(CommandSender sender, Rank rank)
    {
        return getRank(sender) == rank;
    }
}
